package alocation.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import alocation.business.BusinessService;
import alocation.business.UserService;
import alocation.business.VehicleService;
import alocation.business.impl.BusinessServiceImpl;
import alocation.business.impl.UserServiceImpl;
import alocation.business.impl.VehicleServiceImpl;
import alocation.data.DatabaseSingleton;
import alocation.ui.actions.MainApp;
import alocation.ui.actions.ManagerClientMacroAction;
import alocation.ui.actions.CreateReservationMacroAction;
import alocation.util.GUIUtils;
import alocation.util.TextManager;

public class App extends JFrame implements MainApp,ActionListener{

	private VehicleService vehicleService;
	private BusinessService businessService;
	private UserService userService;
	
	private JMenuBar jmMenuPrincipal = new JMenuBar();
	private TextManager textManager = GUIUtils.INSTANCE.getTextManager();
	
	private JMenu jmCadastro = new JMenu(textManager.getText("menu.registers"));	
	private JMenuItem jmCadCliente = new JMenuItem(textManager.getText("menu.client"));
	private JMenuItem jmCadCarro = new JMenuItem(textManager.getText("menu.vehicle"));
	private JMenuItem jmCadFuncionario = new JMenuItem(textManager.getText("menu.employee"));
	
	private JMenu jmReservations = new JMenu(textManager.getText("menu.reservations"));
	private JMenuItem jmNewReservation = new JMenuItem(textManager.getText("menu.new.reservation"));
	
	//cria objeto jdPane, do tipo JDesktopPane. Ele vai dentro d JFrame
	private  JDesktopPane jdPane = new JDesktopPane();


	private CreateReservationMacroAction reservationMacroAction;
	private ManagerClientMacroAction managerClientMacroAction;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		vehicleService = new VehicleServiceImpl(DatabaseSingleton.getINSTANCE());
		businessService = new BusinessServiceImpl(DatabaseSingleton.getINSTANCE());
		userService = new UserServiceImpl(DatabaseSingleton.getINSTANCE());
		//userService = new UserSe;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//"pegue o conteudo do painel" - adiciona o jDesktopPane ao JFrame (janela principal
		   getContentPane().add(jdPane);

		   //adiciona o menu cadastro a barra de menus (jmPrincipal)
		   jmMenuPrincipal.add(jmCadastro);
		   jmMenuPrincipal.add(jmReservations);
		   
		   //adiciona o item cliente ao menu cadastro
		   jmCadastro.add(jmCadCliente);
		   jmCadastro.add(jmCadCarro);
		   jmCadastro.add(jmCadFuncionario);
		   
		   jmReservations.add(jmNewReservation);
		   //"ajusta" a barra de menu dentro da janela principal
		   setJMenuBar(jmMenuPrincipal);

		   //adiciona actionlistener ao item "cliente" do menu,
		   //para que os eventos sejam tratados
		   jmCadCliente.addActionListener(this);
		   jmCadCarro.addActionListener(this);
		   jmCadFuncionario.addActionListener(this);
		   jmNewReservation.addActionListener(this);
		   setSize(800,600);
		   setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		   setVisible(true);
	}
	
	public JDesktopPane getJdPane() {
		jdPane.repaint();
		return jdPane;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == jmNewReservation){
			reservationMacroAction = new CreateReservationMacroAction(textManager,this, vehicleService,userService,businessService);
			reservationMacroAction.execute();
		}
		if(e.getSource() == jmCadCliente){
			managerClientMacroAction = new ManagerClientMacroAction(textManager, this, businessService, userService);
			managerClientMacroAction.execute();
		}
		
	}

	@Override
	public JFrame getFrame() {
		return this;
	}

}
