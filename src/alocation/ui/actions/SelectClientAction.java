package alocation.ui.actions;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.towel.el.annotation.AnnotationResolver;
import com.towel.swing.table.ObjectTableModel;

import alocation.business.UserService;
import alocation.business.domain.Client;
import alocation.business.domain.EnumCategory;
import alocation.business.domain.Period;
import alocation.business.domain.Vehicle;
import alocation.util.TextManager;

public class SelectClientAction extends JInternalFrame implements Action,ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane,panelComandos;
	private JTable table;
	private ObjectTableModel<Client> tableModel;
	private String columnNames = "ID,name";

	private boolean isEnable;
	private MacroAction invoker;
	private TextManager textManager;
	private UserService userService;
	
	private JButton btnClose,btnSelect,btnNew;
	/**
	 * Create the frame.
	 * 
	 * @param newBooking
	 * @param business
	 * @param category
	 */
	public SelectClientAction(MacroAction invoker, TextManager textManager, UserService userService) {
		this.invoker = invoker;
		this.textManager = textManager;
		this.userService = userService;

	}

	protected void initContent() {

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, textManager.getText("title.selection.avaibility.vehicles"),
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel);

		createJTable();
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		scrollPane.setPreferredSize(new Dimension(470, 300));
		panel.add(scrollPane);
		panelComandos = new JPanel();
		contentPane.add(panelComandos);

		addButtons();
	}

	protected void createJTable() {
		// Here we create the resolver for annotated classes.
		AnnotationResolver resolver = new AnnotationResolver(Client.class);
		// We use the resolver as parameter to the ObjectTableModel
		// and the String represent the cols.
		tableModel = new ObjectTableModel<Client>(resolver, columnNames);
		tableModel.setEditableDefault(false);
		// Here we use the list to be the data of the table.
		tableModel.setData(userService.getAllClients());
		table = new JTable(tableModel);
		table.setRowSelectionInterval(0, 0);
		table.sizeColumnsToFit(1);
	}

	@Override
	public boolean isEnable() {
		return isEnable;
	}

	@Override
	public void execute() {
		initContent();
		invoker.getContainer().getJdPane().add(this);
		setVisible(true);

	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	@Override
	public void cancel() {
		dispose();

	}

	private void addButtons() {
		btnClose = new JButton(textManager.getText("button.close"));
		btnClose.addActionListener(this);
		btnNew = new JButton(textManager.getText("button.new"));
		btnNew.addActionListener(this);
		btnSelect = new JButton(textManager.getText("button.select"));
		btnSelect.addActionListener(this); 
		panelComandos.add(btnClose);
		panelComandos.add(btnNew);
		panelComandos.add(btnSelect);
	}
	
	private void doSelect(){
		Client client = tableModel.getData().get(table.getSelectedRow());
		invoker.addParam(client);
		hide();
		invoker.executeNext();
	}
	
	private void doNew(){
		Client client = new Client();
		invoker.addParam(client);
		hide();
		invoker.executeNext();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnSelect) doSelect();
		else if(e.getSource()==btnNew) doNew();
		else invoker.cancel();;
	}

}
