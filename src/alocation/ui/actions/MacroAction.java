package alocation.ui.actions;

import alocation.ui.App;

public interface MacroAction extends Action {

	public void add(Action action);
	public void remove(Action action);
	public void executeNext();
	public void cancel();
	public void setContainer(MainApp container);
	public MainApp getContainer();
	public void addParam(Object o);
	public Object getParam(Class c);
}
