package alocation.ui.actions;

import alocation.business.domain.User;

public class ActionSupport {

	private User session;

	public ActionSupport() {}
	
	public ActionSupport(User session){
		this.session = session;
	}
	
	public User getSession() {
		return session;
	}

	public void setSession(User session) {
		this.session = session;
	}
	
	
}
