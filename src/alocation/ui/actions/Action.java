package alocation.ui.actions;

public interface Action {

	public boolean isEnable();
	public void execute();
	public void setEnable(boolean isEnable);
	public void cancel();
}
