package alocation.ui.actions;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import com.towel.el.annotation.AnnotationResolver;
import com.towel.swing.table.ObjectTableModel;

import alocation.business.BusinessService;
import alocation.business.UserService;
import alocation.business.VehicleService;
import alocation.business.domain.Client;
import alocation.business.domain.Reservation;
import alocation.business.domain.Vehicle;
import alocation.util.TextManager;

public class ShowReservationsAction extends JInternalFrame implements MacroAction, ActionListener {

	private JTable table;
	private ObjectTableModel<Reservation> tableModel;

	private TextManager textManager;
	private MacroAction invoker;
	private List<Reservation> reservationList;
	private String columnNames = "ID,state,contractPeriod:startDate,contractPeriod:endDate,contractPeriod:location,client:name";
	
	private Map<Class,Object> params = new HashMap<>();
	private Iterator<Action> actionQueue;
	private List<Action> actions = new ArrayList<Action>();
	private MainApp container;
	
	
	private JPanel panelCommands, panelContent,mainPanel;
	private JButton btnRentCar, btnClose, btnEdit, btnApprove, btnDeny, btnCancel;

	private BusinessService businessService;
	private UserService userService;
	private VehicleService vehicleService;
	private boolean isEnable;


	public ShowReservationsAction(MacroAction invoker, TextManager textManager, BusinessService businessService,
			UserService userService, VehicleService vehicleService, List<Reservation> reservationList) {
		this.invoker = invoker;
		this.businessService = businessService;
		this.userService = userService;
		this.vehicleService = vehicleService;
		this.textManager = textManager;
		this.reservationList = reservationList;
	}

	public ShowReservationsAction(MainApp container, TextManager textManager, BusinessService businessService,
			UserService userService, VehicleService vehicleService, List<Reservation> reservationList) {
		this.container = container;
		this.businessService = businessService;
		this.userService = userService;
		this.vehicleService = vehicleService;
		this.textManager = textManager;
		this.reservationList = reservationList;
	}

	protected void initContent() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		setLayout(new GridLayout());
		mainPanel = new JPanel();
		panelContent = new JPanel();
		panelCommands = new JPanel();
		

		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(mainPanel);
		mainPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		mainPanel.add(panelContent);
		panelContent.setLayout(new GridLayout(0, 1, 0, 0));


		table = new JTable();
		panelContent.add(table);

		panelCommands.setLayout(new GridLayout(2, 3));
		mainPanel.add(panelCommands);

		panelCommands.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		addButtons();
		
	}

	
	private void addButtons() {
		btnClose = new JButton(textManager.getText("button.close"));
		btnClose.addActionListener(this);
		panelCommands.add(btnClose);
		btnRentCar = new JButton(textManager.getText("button.rent"));
		btnRentCar.addActionListener(this);
		panelCommands.add(btnRentCar);
		btnEdit = new JButton(textManager.getText("button.edit"));
		btnEdit.addActionListener(this);
		panelCommands.add(btnEdit);
		btnApprove = new JButton(textManager.getText("button.approve"));
		btnApprove.addActionListener(this);
		panelCommands.add(btnApprove);
		btnDeny = new JButton(textManager.getText("button.deny"));
		btnDeny.addActionListener(this);
		panelCommands.add(btnDeny);
		btnCancel = new JButton(textManager.getText("button.cancel"));
		btnCancel.addActionListener(this);
		panelCommands.add(btnCancel);
	}

	private void createJTable() {
		// Here we create the resolver for annotated classes.
		AnnotationResolver resolver = new AnnotationResolver(Reservation.class);
		// We use the resolver as parameter to the ObjectTableModel
		// and the String represent the cols.
		tableModel = new ObjectTableModel<Reservation>(resolver, columnNames);
		tableModel.setEditableDefault(false);
		// Here we use the list to be the data of the table.
		tableModel.setData(reservationList);
		table = new JTable(tableModel);
		table.setRowSelectionInterval(0, 0);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnEdit) doEdit();
		else if (e.getSource() == btnApprove) doApprove();
		else if (e.getSource() == btnDeny) doDeny();
		else if (e.getSource() == btnCancel) doCancel();
		else if (e.getSource() == btnRentCar) doRentCar();
		else invoker.cancel();

	}

	public void doEdit(){
		System.out.println("Edit");
	}
	
	public void doApprove(){
		
	}
	
	public void doDeny(){
		
	}
	
	public void doCancel(){
		
	}
	
	public void doRentCar(){
		
	}
/*
	public void doSave() {
		if (checkMandatoryFields()) {
			try {
				if (client == null)
					client = new Client();
				client.setName(fieldName.getText());
				client.setEmail(fieldEmail.getText());
				client.setCPF(fieldNoCPF.getText());
				client.setCNH(fieldNoCNH.getText());
				client.setPhone(fieldPhone.getText());
				client.setAddress(fieldAddress.getText());
				client.setPassword(fieldPassword.getText());
				if (client.getID() == null){
					invoker.addParam(userService.createNewClient(client));
				}
				GUIUtils.INSTANCE.showMessage(invoker.getContainer().getFrame(),
						textManager.getText("application.title"), textManager.getText("message.success.register"),
						JOptionPane.INFORMATION_MESSAGE);
				hide();
				invoker.executeNext();
			} catch (BusinessException be) {
				GUIUtils.INSTANCE.showMessage(invoker.getContainer().getFrame(), be.getMessage(), be.getArgs(),
						JOptionPane.WARNING_MESSAGE);
			} catch (Exception exc) {
				GUIUtils.INSTANCE.handleUnexceptedError(invoker.getContainer().getFrame(), exc);
			}
		}
	}*/

	public void doDelete() {

	}

	@Override
	public boolean isEnable() {
		return isEnable;
	}

	@Override
	public void execute() {

	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;

	}

	@Override
	public void add(Action action) {
		this.actions.add(action);

	}

	@Override
	public void remove(Action action) {
		this.actions.remove(action);

	}

	@Override
	public void executeNext() {
		this.container.getJdPane().repaint();
		if(actionQueue.hasNext()) actionQueue.next().execute();
	}

	@Override
	public void cancel() {
		if(!actions.isEmpty()){	
			for (Action action : actions) {
				action.cancel();
			}
			actions = new ArrayList<Action>();
		}
	}

	public MainApp getContainer() {
		return container;
	}

	public void setContainer(MainApp container) {
		this.container = container;
	}

	@Override
	public void addParam(Object o) {
		params.put(o.getClass(), o);	
	}

	@Override
	public Object getParam(Class c) {
		return params.get(c);
	}
}
