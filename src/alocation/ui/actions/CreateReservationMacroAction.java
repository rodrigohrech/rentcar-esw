package alocation.ui.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import alocation.business.BusinessService;
import alocation.business.UserService;
import alocation.business.VehicleService;
import alocation.business.domain.Client;
import alocation.util.TextManager;

public class CreateReservationMacroAction extends ActionSupport implements MacroAction {

	private boolean isEnable;
	private List<Action> actions = new ArrayList<Action>();
	@SuppressWarnings("rawtypes")
	private Map<Class,Object> params = new HashMap<>();
	private Iterator<Action> actionQueue;
	
	private VehicleService vehicleService;
	private UserService userService;
	private BusinessService businessService;
	private MainApp container;
	private TextManager textManager;
	
	private SelectClientAction selectClient;
	private SelectPeriodAction selectPeriod;
	private SelectVehicleAction selectVehicle;
	private ShowClientAction showClient;
	private CreateReservationAction createReservation;
	
	
	public CreateReservationMacroAction(TextManager textManager, MainApp container, VehicleService vehicleService, UserService userService, BusinessService businessService) {
		this.setContainer(container);
		this.vehicleService = vehicleService;
		this.userService = userService;
		this.businessService = businessService;
		this.textManager = textManager;
	}
	
	@Override
	public boolean isEnable() {
		return this.isEnable;
	}

	@Override
	public void execute() {
		if(getSession()==null) selectClient = new SelectClientAction(this, textManager, userService);
		selectPeriod = new SelectPeriodAction(this, textManager, businessService);
		selectVehicle = new SelectVehicleAction(this, textManager, vehicleService, SelectVehicleAction.BY_AVAIBILITY_TO_RESERVATION);
		createReservation = new CreateReservationAction(this, textManager, businessService);
		actions.add(selectClient);
		actions.add(selectPeriod);
		actions.add(selectVehicle);
		actions.add(createReservation);
		actionQueue = actions.iterator();
		executeNext();
		
	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	@Override
	public void add(Action action) {
		this.actions.add(action);
	}

	@Override
	public void remove(Action action) {
		this.actions.remove(action);
	}

	@Override
	public void executeNext() {
		this.container.getJdPane().repaint();
		Client client = (Client) getParam(Client.class);
		if(client!=null){
			if(client.getID()==null) {
				showClient = new ShowClientAction(this, textManager, businessService, userService);
				showClient.execute();
			} else if(actionQueue.hasNext()) actionQueue.next().execute();
		} else if(actionQueue.hasNext()) actionQueue.next().execute();
	}

	@Override
	public void cancel() {
		if(!actions.isEmpty()){	
			for (Action action : actions) {
				action.cancel();
			}
			actions = new ArrayList<Action>();
		}
		
	}

	public MainApp getContainer() {
		return container;
	}

	public void setContainer(MainApp container) {
		this.container = container;
	}

	@Override
	public void addParam(Object o) {
		params.put(o.getClass(), o);	
	}

	@Override
	public Object getParam(Class c) {
		return params.get(c);
	}

	
	
}
