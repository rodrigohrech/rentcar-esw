package alocation.ui.actions;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;

public interface MainApp  {

	public JDesktopPane getJdPane();
	public JFrame getFrame();
}
