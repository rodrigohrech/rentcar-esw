package alocation.ui.actions;

import java.util.Date;

import javax.swing.JOptionPane;

import alocation.business.BusinessException;
import alocation.business.BusinessService;
import alocation.business.domain.Client;
import alocation.business.domain.Payment;
import alocation.business.domain.Period;
import alocation.business.domain.Vehicle;
import alocation.util.GUIUtils;
import alocation.util.TextManager;

public class CreateReservationAction implements Action {

	private boolean isEnable;

	private Client client;
	private Vehicle vehicle;
	private Payment payment;
	private BusinessService businessService;
	private MacroAction invoker;
	private TextManager textManager;
	private Period period;

	public CreateReservationAction(CreateReservationMacroAction invoker, TextManager textManager,
			BusinessService businessService) {
		this.businessService = businessService;
		this.invoker = invoker;
		this.textManager = textManager;
	}

	@Override
	public boolean isEnable() {
		return isEnable;
	}

	@Override
	public void execute() {
		try {
			client = (Client) invoker.getParam(Client.class);
			vehicle = (Vehicle) invoker.getParam(Vehicle.class);
			period = (Period) invoker.getParam(Period.class);

			payment = new Payment();
			payment.setClient(client);
			payment.setVehicle(vehicle);
			if (askPayment()) {
				payment.setDate(new Date());
			}
			GUIUtils.INSTANCE.showMessage(invoker.getContainer().getFrame(),textManager.getText("application.title"),textManager.getText("message.success.reservation"),JOptionPane.INFORMATION_MESSAGE);
			businessService.reservateVehicle(client, vehicle, payment, period);
			
		} catch (BusinessException be) {
			GUIUtils.INSTANCE.showMessage(invoker.getContainer().getFrame(), be.getMessage(), be.getArgs(),
					JOptionPane.WARNING_MESSAGE);
		} catch (Exception exc) {
			GUIUtils.INSTANCE.handleUnexceptedError(invoker.getContainer().getFrame(), exc);
		}

	}

	public boolean askPayment() {
		boolean flag = false;
		if (JOptionPane.showConfirmDialog(invoker.getContainer().getFrame(), textManager.getText("message.ask.payment"),
				textManager.getText("application.title"), JOptionPane.YES_NO_OPTION) > 0)
			flag = true;

		return flag;
	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;

	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub

	}

}
