package alocation.ui.actions;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import alocation.business.BusinessException;
import alocation.business.BusinessService;
import alocation.business.domain.EnumCity;
import alocation.business.domain.Period;
import alocation.util.GUIUtils;
import alocation.util.TextManager;

public class SelectPeriodAction extends JInternalFrame implements Action {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2616640526399977663L;
	private boolean isEnable;
	private MacroAction invoker;
	private TextManager textManager;
	private BusinessService businessService;

	private JPanel contentPane;
	private JPanel panelRetirada;
	private JFormattedTextField txtDataHoraDevolucao;
	private JComboBox cbCidadeRetirada;
	private JFormattedTextField txtDataHoraRetirada;

	/**
	 * Create the frame.
	 * 
	 * @param bm
	 */
	public SelectPeriodAction(MacroAction invoker, TextManager textManager, BusinessService businessService) {
		super("Selecionar Periodo", false, false, false);
		this.textManager = textManager;
		this.invoker = invoker;
		this.businessService = businessService;
	}

	protected void initContent() {

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));

		panelRetirada = new JPanel();
		panelRetirada.setBorder(new TitledBorder(null, textManager.getText("field.withdrawal"), TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		contentPane.add(panelRetirada);
		panelRetirada.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblCidade = new JLabel(textManager.getText("field.city"));
		panelRetirada.add(lblCidade);

		cbCidadeRetirada = new JComboBox();
		cbCidadeRetirada.setModel(new DefaultComboBoxModel(EnumCity.getAllCities()));
		panelRetirada.add(cbCidadeRetirada);

		// final String date = GUIUtils.DATE_TIME_FORMAT.format(new Date());

		JLabel lblDatahora = new JLabel(textManager.getText("field.datetime"));
		panelRetirada.add(lblDatahora);
		txtDataHoraRetirada = new JFormattedTextField(GUIUtils.DATE_TIME_FORMAT);
		txtDataHoraRetirada.setToolTipText(GUIUtils.DATE_TIME_FORMAT.toPattern());
		txtDataHoraRetirada.setColumns(10);
		panelRetirada.add(txtDataHoraRetirada);

		JPanel panelDevolucao = new JPanel();
		panelDevolucao.setBorder(new TitledBorder(null, textManager.getText("field.devolution"), TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		contentPane.add(panelDevolucao);

		JLabel lblDataHora2 = new JLabel("Data/Hora");
		panelDevolucao.add(lblDataHora2);

		txtDataHoraDevolucao = new JFormattedTextField(GUIUtils.DATE_TIME_FORMAT);
		txtDataHoraDevolucao.setToolTipText(GUIUtils.DATE_TIME_FORMAT.toPattern());
		txtDataHoraDevolucao.setColumns(10);
		panelDevolucao.add(txtDataHoraDevolucao);

		JPanel panelComandos = new JPanel();
		contentPane.add(panelComandos);
		panelComandos.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 2));

		JButton btnCancelar = new JButton(textManager.getText("button.cancel"));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				invoker.cancel();
			}
		});
		panelComandos.add(btnCancelar);

		JButton btnProximo = new JButton(textManager.getText("button.next"));
		btnProximo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				createPeriod();

			}
		});
		panelComandos.add(btnProximo);

	}

	private void createPeriod() {
		try {
			EnumCity location = EnumCity.byType(cbCidadeRetirada.getSelectedItem().toString());
			Date startDate = (Date) txtDataHoraRetirada.getValue();
			Date endDate = (Date) txtDataHoraDevolucao.getValue();

			hide();
			invoker.addParam(businessService.createPeriod(startDate, endDate, location));
			invoker.executeNext();
		} catch (BusinessException be) {
			GUIUtils.INSTANCE.showMessage(invoker.getContainer().getFrame(), be.getMessage(), be.getArgs(),
					JOptionPane.WARNING_MESSAGE);
		} catch (Exception exc) {
			GUIUtils.INSTANCE.handleUnexceptedError(invoker.getContainer().getFrame(), exc);
		}
	}

	@Override
	public boolean isEnable() {
		return this.isEnable;
	}

	@Override
	public void execute() {
		initContent();
		invoker.getContainer().getJdPane().add(this);
		setVisible(true);
	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;

	}

	@Override
	public void cancel() {
		dispose();
	}

}