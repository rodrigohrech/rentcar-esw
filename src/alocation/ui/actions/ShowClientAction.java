package alocation.ui.actions;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.towel.swing.table.ObjectTableModel;

import alocation.business.BusinessException;
import alocation.business.BusinessService;
import alocation.business.UserService;
import alocation.business.domain.Client;
import alocation.business.domain.Vehicle;
import alocation.util.GUIUtils;
import alocation.util.TextManager;

public class ShowClientAction extends JInternalFrame implements Action, ActionListener {

	private JTable table;
	private ObjectTableModel<Vehicle> tableModel;

	private TextManager textManager;
	private MacroAction invoker;
	private Client client;

	private JPanel panel1, panel2, panel3, panel4, panel5, panel6, panel7, panelTable, panelCommands, panelContent,
			mainPanel;
	private JButton btnRentCar, btnClose, btnSave, btnDelete;
	private JLabel lblName, lblNoCPF, lblNoCNH, lblAddress, lblPhone, lblEmail, lblPassword;
	private JTextField fieldName, fieldNoCPF, fieldNoCNH, fieldAddress, fieldPhone, fieldEmail, fieldPassword;

	private BusinessService businessService;
	private UserService userService;

	public ShowClientAction(MacroAction invoker, TextManager textManager, BusinessService businessService,
			UserService userService) {
		this.invoker = invoker;
		this.businessService = businessService;
		this.userService = userService;
		this.textManager = textManager;
	}

	@Override
	public boolean isEnable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void execute() {
		this.client = (Client) invoker.getParam(Client.class);
		initContent();
		invoker.getContainer().getJdPane().add(this);
		setVisible(true);
		mainPanel.revalidate();
	}

	protected void initContent() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		setLayout(new GridLayout());
		mainPanel = new JPanel();
		panelContent = new JPanel();
		panelCommands = new JPanel();
		panelTable = new JPanel();
		panel1 = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();
		panel4 = new JPanel();
		panel5 = new JPanel();
		panel6 = new JPanel();
		panel7 = new JPanel();

		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(mainPanel);
		mainPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		mainPanel.add(panelContent);
		panelContent.setLayout(new GridLayout(0, 1, 0, 0));

		panelContent.add(panel1);

		panelContent.add(panel2);

		panelContent.add(panel3);

		panelContent.add(panel4);

		panelContent.add(panel5);

		panelContent.add(panel6);

		panelContent.add(panel7);

		panelContent.add(panelTable);

		table = new JTable();
		panelTable.add(table);

		panelCommands.setLayout(new GridLayout(2, 3));
		mainPanel.add(panelCommands);

		panelCommands.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		addButtons();
		addLabels();
		addFields();
		setLayoutToSubcomponents(new FlowLayout(FlowLayout.LEFT, 5, 5));
		populateFields();
	}

	private void populateFields() {
		if (client != null) {
			fieldName.setText(client.getName());
			fieldNoCPF.setText(client.getCPF());
			fieldNoCNH.setText(client.getCNH());
			fieldAddress.setText(client.getAddress());
			fieldPhone.setText(client.getPhone());
			fieldEmail.setText(client.getEmail());
			fieldPassword.setText(client.getPassword());
		}
	}

	private void addLabels() {
		lblName = new JLabel(textManager.getText("field.name"));
		panel1.add(lblName);
		lblNoCPF = new JLabel(textManager.getText("field.cpf"));
		panel2.add(lblNoCPF);
		lblNoCNH = new JLabel(textManager.getText("field.cnh"));
		panel3.add(lblNoCNH);
		lblAddress = new JLabel(textManager.getText("field.address"));
		panel4.add(lblAddress);
		lblPhone = new JLabel(textManager.getText("field.phone"));
		panel5.add(lblPhone);
		lblEmail = new JLabel(textManager.getText("field.email"));
		panel6.add(lblEmail);
		lblPassword = new JLabel(textManager.getText("field.password"));
		panel7.add(lblPassword);
	}

	private void addFields() {
		int columns = 20;
		fieldName = new JTextField(columns);
		panel1.add(fieldName);
		fieldNoCPF = new JTextField(columns);
		panel2.add(fieldNoCPF);
		fieldNoCNH = new JTextField(columns);
		panel3.add(fieldNoCNH);
		fieldAddress = new JTextField(columns);
		panel4.add(fieldAddress);
		fieldPhone = new JTextField(columns);
		panel5.add(fieldPhone);
		fieldEmail = new JTextField(columns);
		panel6.add(fieldEmail);
		fieldPassword = new JTextField(columns);
		panel7.add(fieldPassword);
	}

	private void setLayoutToSubcomponents(LayoutManager layout) {
		panel1.setLayout(layout);
		panel2.setLayout(layout);
		panel3.setLayout(layout);
		panel4.setLayout(layout);
		panel5.setLayout(layout);
		panel6.setLayout(layout);
		panel7.setLayout(layout);
	}

	private void addButtons() {
		btnClose = new JButton(textManager.getText("button.close"));
		btnClose.addActionListener(this);
		panelCommands.add(btnClose);
		btnRentCar = new JButton(textManager.getText("button.rent"));
		btnRentCar.addActionListener(this);
		panelCommands.add(btnRentCar);
		btnDelete = new JButton(textManager.getText("button.delete"));
		btnDelete.addActionListener(this);
		panelCommands.add(btnDelete);
		btnSave = new JButton(textManager.getText("button.save"));
		btnSave.addActionListener(this);
		panelCommands.add(btnSave);
	}

	@Override
	public void setEnable(boolean isEnable) {
		// TODO Auto-generated method stub

	}

	@Override
	public void cancel() {
		dispose();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnSave) doSave();
		else if (e.getSource() == btnDelete)
			System.out.println("Delete");
		else if (e.getSource() == btnRentCar)
			System.out.println("Rent");
		else invoker.cancel();

	}

	private boolean checkMandatoryFields() {
		if (!GUIUtils.INSTANCE.checkMandatoryString(invoker.getContainer().getFrame(), fieldName.getText(),
				"field.name"))
			return false;
		if (!GUIUtils.INSTANCE.checkMandatoryString(invoker.getContainer().getFrame(), fieldEmail.getText(),
				"field.email"))
			return false;
		if (!GUIUtils.INSTANCE.checkMandatory(invoker.getContainer().getFrame(), fieldNoCPF.getText(), "field.cpf"))
			return false;
		if (!GUIUtils.INSTANCE.checkMandatory(invoker.getContainer().getFrame(), fieldNoCNH.getText(), "field.cnh"))
			return false;
		if (!GUIUtils.INSTANCE.checkMandatory(invoker.getContainer().getFrame(), fieldPhone.getText(), "field.phone"))
			return false;
		if (!GUIUtils.INSTANCE.checkMandatory(invoker.getContainer().getFrame(), fieldAddress.getText(),
				"field.address"))
			return false;
		if (!GUIUtils.INSTANCE.checkMandatory(invoker.getContainer().getFrame(), fieldPassword.getText(),
				"field.password"))
			return false;
		return true;
	}

	public void doSave() {
		if (checkMandatoryFields()) {
			try {
				if (client == null)
					client = new Client();
				client.setName(fieldName.getText());
				client.setEmail(fieldEmail.getText());
				client.setCPF(fieldNoCPF.getText());
				client.setCNH(fieldNoCNH.getText());
				client.setPhone(fieldPhone.getText());
				client.setAddress(fieldAddress.getText());
				client.setPassword(fieldPassword.getText());
				if (client.getID() == null){
					invoker.addParam(userService.createNewClient(client));
				}
				GUIUtils.INSTANCE.showMessage(invoker.getContainer().getFrame(),
						textManager.getText("application.title"), textManager.getText("message.success.register"),
						JOptionPane.INFORMATION_MESSAGE);
				hide();
				invoker.executeNext();
			} catch (BusinessException be) {
				GUIUtils.INSTANCE.showMessage(invoker.getContainer().getFrame(), be.getMessage(), be.getArgs(),
						JOptionPane.WARNING_MESSAGE);
			} catch (Exception exc) {
				GUIUtils.INSTANCE.handleUnexceptedError(invoker.getContainer().getFrame(), exc);
			}
		}
	}

	public void doDelete() {

	}

}
