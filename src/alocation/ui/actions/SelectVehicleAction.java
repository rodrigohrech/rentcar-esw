package alocation.ui.actions;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.towel.el.annotation.AnnotationResolver;
import com.towel.swing.table.ObjectTableModel;

import alocation.business.VehicleService;
import alocation.business.domain.EnumCategory;
import alocation.business.domain.Period;
import alocation.business.domain.Vehicle;
import alocation.util.TextManager;

public class SelectVehicleAction extends JInternalFrame implements Action {

	/**
	 * 
	 */
	public static final int ALL = 0;
	public static final int BY_AVAIBILITY_TO_RENT = 1;
	public static final int BY_AVAIBILITY_TO_RESERVATION = 2;

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private ObjectTableModel<Vehicle> tableModel;
	private List<Vehicle> vehiclesList = new ArrayList<>();
	String columnNames = "ID,licensePlate,model,year,color,brand,engine";

	private Period period;
	private boolean isEnable;
	private int filter;
	private MacroAction invoker;
	private TextManager textManager;
	private VehicleService vehicleService;
	private EnumCategory category;

	/**
	 * Create the frame.
	 * 
	 * @param newBooking
	 * @param business
	 * @param category
	 */
	public SelectVehicleAction(MacroAction invoker, TextManager textManager, VehicleService vehicleService,
			int filter) {
		this.invoker = invoker;
		this.textManager = textManager;
		this.filter = filter;
		this.vehicleService = vehicleService;
		this.vehiclesList = new ArrayList<>();

	}

	protected void initContent() {

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, textManager.getText("title.selection.avaibility.vehicles"),
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(panel);

		createJTable();
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		scrollPane.setPreferredSize(new Dimension(470, 300));
		panel.add(scrollPane);
		JPanel panelComandos = new JPanel();
		contentPane.add(panelComandos);

		final JCheckBox chckbxPagamento = new JCheckBox("Pagamento");
		panelComandos.add(chckbxPagamento);

		JButton btnVoltar = new JButton(textManager.getText("button.back"));
		panelComandos.add(btnVoltar);

		JButton btnSelect = new JButton(textManager.getText("button.select"));
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vehicle vehicle = tableModel.getData().get(table.getSelectedRow());
				invoker.addParam(vehicle);
				hide();
				invoker.executeNext();
			}
		});
		panelComandos.add(btnSelect);
	}

	protected void createJTable() {
		// Here we create the resolver for annotated classes.
		AnnotationResolver resolver = new AnnotationResolver(Vehicle.class);
		// We use the resolver as parameter to the ObjectTableModel
		// and the String represent the cols.
		tableModel = new ObjectTableModel<Vehicle>(resolver, columnNames);
		tableModel.setEditableDefault(false);
		// Here we use the list to be the data of the table.
		tableModel.setData(vehiclesList);
		table = new JTable(tableModel);
		table.setRowSelectionInterval(0, 0);
	}

	private EnumCategory getCategory() {
		EnumCategory c = null;
		String[] possibilities = EnumCategory.getAllCategories();
		String s = (String) JOptionPane.showInputDialog(this, textManager.getText("message.choose.category"),
				textManager.getText("title.selection.category"), JOptionPane.PLAIN_MESSAGE, null, possibilities,
				possibilities[0]);

		if (s == null || s.length() == 0)
			invoker.cancel();
		else
			c = EnumCategory.byType(s);
		return c;
	}

	private void populateList() {
		vehiclesList.clear();
		switch (filter) {
		case 0:
			vehiclesList = vehicleService.getAllVehicles();
			break;
		case 1:
			this.category = getCategory();
			this.period = (Period) invoker.getParam(Period.class);
			if (category != null && period != null)
				this.vehiclesList = vehicleService.getAllVehiclesAvaibleToRent(category, period);
			break;
		case 2:
			this.category = getCategory();
			this.period = (Period) invoker.getParam(Period.class);
			if (category != null && period != null)
				this.vehiclesList = vehicleService.getAllVehiclesAvaibleToReservate(category, period);
			break;
		default:
			vehiclesList = vehicleService.getAllVehicles();
			break;
		}
	}

	@Override
	public boolean isEnable() {
		return isEnable;
	}

	@Override
	public void execute() {
		populateList();
		if (!vehiclesList.isEmpty()) {
			initContent();
			invoker.getContainer().getJdPane().add(this);
			setVisible(true);
		}
	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	@Override
	public void cancel() {
		dispose();

	}

}
