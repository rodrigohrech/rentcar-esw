package alocation.ui.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import alocation.business.BusinessService;
import alocation.business.UserService;
import alocation.business.VehicleService;
import alocation.business.domain.User;
import alocation.util.TextManager;

public class ManagerClientMacroAction extends ActionSupport implements MacroAction {

	
	private boolean isEnable;
	private Map<Class,Object> params = new HashMap<>();
	private Iterator<Action> actionQueue;
	private List<Action> actions = new ArrayList<Action>();
	private MainApp container;
	private TextManager textManager;
	
	private UserService userService;
	private BusinessService businessService;
	
	private SelectClientAction selectClient;	
	private ShowClientAction showClientAction;
	
	public ManagerClientMacroAction(TextManager textManager, MainApp container, BusinessService businessService, UserService userService) {
		this.textManager = textManager;
		this.container = container;
		this.userService = userService;
		this.businessService = businessService;
	}

	public ManagerClientMacroAction(User session) {
		super(session);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isEnable() {
		return isEnable;
	}

	@Override
	public void execute() {
		actions.clear();
		selectClient = new SelectClientAction(this, textManager, userService);
		showClientAction = new ShowClientAction(this, textManager, businessService, userService);
		add(selectClient);
		add(showClientAction);
		actionQueue = actions.iterator();
		executeNext();

	}

	@Override
	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;

	}

	@Override
	public void add(Action action) {
		this.actions.add(action);

	}

	@Override
	public void remove(Action action) {
		this.actions.remove(action);

	}

	@Override
	public void executeNext() {
		this.container.getJdPane().repaint();
		if(actionQueue.hasNext()) actionQueue.next().execute();
	}

	@Override
	public void cancel() {
		if(!actions.isEmpty()){	
			for (Action action : actions) {
				action.cancel();
			}
			actions = new ArrayList<Action>();
		}
	}

	public MainApp getContainer() {
		return container;
	}

	public void setContainer(MainApp container) {
		this.container = container;
	}

	@Override
	public void addParam(Object o) {
		params.put(o.getClass(), o);	
	}

	@Override
	public Object getParam(Class c) {
		return params.get(c);
	}

}
