package alocation.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;

import com.towel.swing.table.ObjectTableModel;

public abstract class AbstractManageUI extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3403533225655420494L;
	private JPanel panelMain;
	private JPanel panelCommand;
	private JTable table;
	
	/**
	 * Create the frame.
	 */
	public  AbstractManageUI(String title) {
		super(title);
		setClosable(true);
		setSize(300,200);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		JPanel panel = new JPanel();
		panelMain.add(panel, BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		scrollPane.setPreferredSize(new Dimension(300, 200));
		panel.add(scrollPane);

	}

	protected void refreshTable(ObjectTableModel tableModel) {
		table = new JTable(tableModel);
	}
	
	
}
