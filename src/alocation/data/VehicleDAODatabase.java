package alocation.data;

import alocation.business.domain.Vehicle;
import alocation.data.interfaces.VehicleDAO;

public class VehicleDAODatabase extends GenericDAODatabase<Vehicle> implements VehicleDAO {


	@Override
	public Integer getID(Vehicle vehicle) {
		return vehicle.getID();
	}

	@Override
	public void setID(Vehicle element, Integer id) {
		element.setID(id);
		
	}

}
