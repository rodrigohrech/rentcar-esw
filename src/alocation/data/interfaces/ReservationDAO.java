package alocation.data.interfaces;

import alocation.business.domain.Reservation;

public interface ReservationDAO extends GenericDAO<Reservation> {

}
