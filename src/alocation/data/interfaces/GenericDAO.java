package alocation.data.interfaces;

import java.util.List;

public interface GenericDAO<T> {

	public List<T> getAll();
	public T get(T element);
	public void delete(T element);
	public T save(T element);
	public void update(T element);
	public Object getID(T element);
	public void setID(T element, Integer id);
	public void deleteAll();
}
