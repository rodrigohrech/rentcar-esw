package alocation.data.interfaces;

import alocation.business.domain.Employee;

public interface EmployeeDAO extends GenericDAO<Employee> {

}
