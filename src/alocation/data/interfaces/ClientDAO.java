package alocation.data.interfaces;

import alocation.business.domain.Client;

public interface ClientDAO extends GenericDAO<Client> {

}
