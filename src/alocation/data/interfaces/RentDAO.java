package alocation.data.interfaces;

import alocation.business.domain.Rent;

public interface RentDAO extends GenericDAO<Rent> {

}
