package alocation.data.interfaces;

import alocation.business.domain.Vehicle;

public interface VehicleDAO extends GenericDAO<Vehicle> {

}
