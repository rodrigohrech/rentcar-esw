package alocation.data;

import alocation.business.domain.Rent;
import alocation.data.interfaces.RentDAO;

public class RentDAODatabase extends GenericDAODatabase<Rent> implements RentDAO {

	@Override
	public Integer getID(Rent rent) {
		return rent.getID();
	}

	@Override
	public void setID(Rent element, Integer id) {
		element.setID(id);

	}

}
