package alocation.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alocation.data.interfaces.GenericDAO;

public abstract class GenericDAODatabase<T> implements GenericDAO<T> {

	private Map<Object, T> records = new HashMap<>();
	
	@Override
	public List<T> getAll() {
		return new ArrayList<T>(records.values());
	}

	@Override
	public T get(T element) {
		return records.get(getID(element));
	}


	@Override
	public abstract Object getID(T element);

	@Override
	public abstract void setID(T element, Integer id);
	
	@Override
	public void delete(T element) {
		records.remove(getID(element));
	}

	@Override
	public void deleteAll() {
		records.clear();
	}
	
	@Override
	public T save(T element){
		Integer id = getNextIDNumber();
		setID(element, id);
		records.put(id, element);
		return element;
	}

	@Override
	public void update(T element) {
		records.put(getID(element), element);
		
	}

	private Integer getNextIDNumber(){
		return records.size() + 1;
	}
	

}
