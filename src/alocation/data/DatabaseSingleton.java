package alocation.data;

import java.util.HashMap;
import java.util.Map;

import alocation.business.domain.Attendant;
import alocation.business.domain.Client;
import alocation.business.domain.Employee;
import alocation.business.domain.EnumCategory;
import alocation.business.domain.EnumCity;
import alocation.business.domain.Manager;
import alocation.business.domain.Vehicle;
import alocation.data.interfaces.ClientDAO;
import alocation.data.interfaces.EmployeeDAO;
import alocation.data.interfaces.GenericDAO;
import alocation.data.interfaces.RentDAO;
import alocation.data.interfaces.ReservationDAO;
import alocation.data.interfaces.VehicleDAO;


public class DatabaseSingleton {

	private Map<Class, GenericDAO> daos = new HashMap<Class, GenericDAO>();
	private static DatabaseSingleton instance;
	
	public static DatabaseSingleton getINSTANCE(){
		if(instance == null){
			synchronized (DatabaseSingleton.class) {
				if(instance == null){
					instance = new DatabaseSingleton() {
					};
				}
			}
		}
		return instance;
	}

	private DatabaseSingleton() {
		daos.put(ClientDAO.class,new ClientDAODatabase());
		daos.put(EmployeeDAO.class,new EmployeeDAODatabase());
		daos.put(RentDAO.class,new RentDAODatabase());
		daos.put(ReservationDAO.class,new ReservationDAODatabase());
		daos.put(VehicleDAO.class,new VehicleDAODatabase());
		
		initData();
	}

	
	@SuppressWarnings("unchecked")
	private void initData() {
		try {
			Client client;
			Attendant employee;
			Vehicle vehicleBasic, vehicleLuxe,vehicleVan;
			for (int i = 0; i < 6; i++) {
				client = new Client("client"+i, "99999999", "Rua do client"+i, "client"+i+"@email.com", "client"+i+"abc", "987654321", "123123123");
				daos.get(ClientDAO.class).save(client);
				employee = new Attendant("employee"+i, "888888888", "Av do employee"+i, "employee"+i+"@email.com", "employee"+i+"abc");
				daos.get(EmployeeDAO.class).save(employee);
			}
			
			for (int i = 0; i < 3; i++) {
				vehicleBasic =  new Vehicle("car"+i, "201"+i,"color"+i, "brand"+i, "1.0", EnumCategory.BASIC,"AAA-000"+i,EnumCity.POA);
				daos.get(VehicleDAO.class).save(vehicleBasic);
				vehicleLuxe =  new Vehicle("car"+(i+6), "201"+i,"color"+i, "brand"+i, "2.0", EnumCategory.LUXE,"BBB-000"+i,EnumCity.POA);
				daos.get(VehicleDAO.class).save(vehicleLuxe);
				vehicleVan =  new Vehicle("car"+(i+12), "201"+i,"color"+i, "brand"+i, "1.0", EnumCategory.VAN,"CCC-000"+i,EnumCity.POA);
				daos.get(VehicleDAO.class).save(vehicleVan);
			}
			for (int i = 9; i < 12; i++) {
				vehicleBasic =  new Vehicle("car"+i, "201"+i,"color"+i, "brand"+i, "1.0", EnumCategory.BASIC,"AAA-000"+i,EnumCity.BSB);
				daos.get(VehicleDAO.class).save(vehicleBasic);
				vehicleLuxe =  new Vehicle("car"+(i+6), "201"+i,"color"+i, "brand"+i, "2.0", EnumCategory.LUXE,"BBB-000"+i,EnumCity.BSB);
				daos.get(VehicleDAO.class).save(vehicleLuxe);
				vehicleVan =  new Vehicle("car"+(i+12), "201"+i,"color"+i, "brand"+i, "1.0", EnumCategory.VAN,"CCC-000"+i,EnumCity.BSB);
				daos.get(VehicleDAO.class).save(vehicleVan);
			}
			for (int i = 12; i < 15; i++) {
				vehicleBasic =  new Vehicle("car"+i, "201"+i,"color"+i, "brand"+i, "1.0", EnumCategory.BASIC,"AAA-000"+i,EnumCity.SAO);
				daos.get(VehicleDAO.class).save(vehicleBasic);
				vehicleLuxe =  new Vehicle("car"+(i+6), "201"+i,"color"+i, "brand"+i, "2.0", EnumCategory.LUXE,"BBB-000"+i,EnumCity.SAO);
				daos.get(VehicleDAO.class).save(vehicleLuxe);
				vehicleVan =  new Vehicle("car"+(i+12), "201"+i,"color"+i, "brand"+i, "1.0", EnumCategory.VAN,"CCC-000"+i,EnumCity.SAO);
				daos.get(VehicleDAO.class).save(vehicleVan);
			}
			//Manager
			Manager manager = new Manager("manager", "5555555555", "Av da UFRGS", "ingrid@email.com", "123123");
			daos.get(EmployeeDAO.class).save(manager);


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public GenericDAO get(Class dao){
		return daos.get(dao);
	}

	public void clearDatabase(){
		if(!daos.isEmpty())
			for (GenericDAO d : daos.values()) {
				d.deleteAll();
			}
	}
}
