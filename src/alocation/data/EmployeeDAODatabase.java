package alocation.data;

import alocation.business.domain.Employee;
import alocation.data.interfaces.EmployeeDAO;

public class EmployeeDAODatabase extends GenericDAODatabase<Employee> implements EmployeeDAO {

	@Override
	public Integer getID(Employee employee) {
		return employee.getID();
	}

	@Override
	public void setID(Employee element, Integer id) {
		element.setID(id);
	}

}
