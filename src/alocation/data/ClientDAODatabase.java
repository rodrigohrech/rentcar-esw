package alocation.data;

import alocation.business.domain.Client;
import alocation.data.interfaces.ClientDAO;

public class ClientDAODatabase extends GenericDAODatabase<Client> implements ClientDAO {

	@Override
	public Integer getID(Client client) {
		return client.getID();
	}

	@Override
	public void setID(Client element, Integer id) {
		element.setID(id);
	}

}
