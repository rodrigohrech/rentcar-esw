package alocation.data;

import alocation.business.domain.Reservation;
import alocation.data.interfaces.ReservationDAO;

public class ReservationDAODatabase extends GenericDAODatabase<Reservation> implements ReservationDAO {

	@Override
	public Integer getID(Reservation reservation) {
		return reservation.getID();
	}

	@Override
	public void setID(Reservation element, Integer id) {
		element.setID(id);
		
	}

}
