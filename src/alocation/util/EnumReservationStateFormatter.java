package alocation.util;

import com.towel.bean.Formatter;

import alocation.business.domain.EnumReservationState;

public class EnumReservationStateFormatter implements Formatter{

	@Override
	public String format(Object arg0) {
		EnumReservationState city = (EnumReservationState) arg0;
		return city.getType();
	}

	@Override
	public String getName() {
		return "enumReservation";
	}

	@Override
	public Object parse(Object arg0) {
		return EnumReservationState.byType((String) arg0);
	}


}
