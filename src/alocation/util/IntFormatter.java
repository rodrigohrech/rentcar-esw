package alocation.util;

import com.towel.bean.Formatter;

public class IntFormatter implements Formatter {

	@Override
    public String format(Object obj) {
        return Integer.toString((Integer) obj);
    }

    @Override
    public String getName() {
        return "int";
    }

	@Override
	public Object parse(Object arg0) {
		return Integer.parseInt(arg0.toString());
	}

}
