package alocation.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.towel.bean.Formatter;

public class DateFormatter implements Formatter {

	private final static SimpleDateFormat formatter = GUIUtils.DATE_TIME_FORMAT;
	
	public DateFormatter() {
		// TODO Auto-generated constructor stub
	}

	@Override
    public String format(Object obj) {
        Date date = (Date) obj;
        return formatter.format(date);
    }

    @Override
    public String getName() {
        return "date";
    }


	@Override
	public Object parse(Object arg0) {
		Date date = new Date();
        try {
            date = formatter.parse(arg0.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
	}

}
