package alocation.util;

import com.towel.bean.Formatter;

import alocation.business.domain.EnumCity;

public class EnumCityFormatter implements Formatter{

	@Override
	public String format(Object arg0) {
		EnumCity city = (EnumCity) arg0;
		return city.getCity();
	}

	@Override
	public String getName() {
		return "enumCity";
	}

	@Override
	public Object parse(Object arg0) {
		return EnumCity.byType((String) arg0);
	}


}
