package alocation.business.domain;

public enum EnumCategory {

	BASIC("Basico"), LUXE("Luxo"), VAN("Mini Van"), ALL("Todos");
	
	private final String type;
	
	EnumCategory(String type) { this.type = type; }
	
	public String getType() { return type; }
	
	public static EnumCategory byType(String type){
		for(EnumCategory types : values()){
			if (type  == types.getType()) return types;
		}
		throw new IllegalArgumentException("Invalid Type");
	}
	
	public static String[] getAllCategories(){
		    EnumCategory[] categories = values();
		    String[] names = new String[categories.length];

		    for (int i = 0; i < categories.length; i++) {
		        names[i] = categories[i].getType();
		    }

		    return names;
		}
}
