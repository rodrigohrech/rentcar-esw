package alocation.business.domain;

import java.util.Calendar;
import java.util.Date;

public class Payment {

	private Integer ID;
	private Date date;
	private Period referencePeriod;
	private Client client;
	private Vehicle vehicle;
	
	public Payment() {}
	
	public Payment(Integer iD) {
		this.ID = iD;
	}

	public Payment(Integer iD, Date date, Float value, Client client, Vehicle vehicle) {
		this.ID = iD;
		this.date = date;
		this.client = client;
		this.vehicle = vehicle;
	}

	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Float getValue() {
		Calendar dateStart = Calendar.getInstance();
		Calendar dateEnd = Calendar.getInstance();
		dateStart.setTime(getReferencePeriod().getStartDate());
		dateEnd.setTime(getReferencePeriod().getEndDate());
		int days = dateEnd.get(Calendar.DAY_OF_YEAR) - dateStart.get(Calendar.DAY_OF_YEAR);
		float value = 0;
		if(vehicle!=null) value = Math.abs(days) * vehicle.getValue();
		return value;
	}

	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Period getReferencePeriod() {
		return referencePeriod;
	}

	public void setReferencePeriod(Period referencePeriod) {
		this.referencePeriod = referencePeriod;
	}
	
	
	
}
