package alocation.business.domain;

import java.util.Date;

import com.towel.el.annotation.Resolvable;

import alocation.util.DateFormatter;

public class Reservation extends Transaction {

	@Resolvable(colName="Estado", formatter=EnumReservationState.class)
	EnumReservationState state;


	public Reservation() {
	}

	public Reservation(Client client, Vehicle vehicle, Payment payment, Period contractPeriod) {
		super(client, vehicle, payment, contractPeriod);
	}

	public EnumReservationState getState() {
		return state;
	}

	public void setState(EnumReservationState state) {
		this.state = state;
	}

	public boolean isPaid() {
		return getPayment().getDate() != null;
	}

	@Override
	public boolean isLate() {
		Date today = new Date();
		return 	getContractPeriod().getEndDate().before(today);
	}

}
