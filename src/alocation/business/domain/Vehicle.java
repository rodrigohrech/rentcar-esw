package alocation.business.domain;

import java.util.ArrayList;
import java.util.List;

import com.towel.el.annotation.Resolvable;

import alocation.util.IntFormatter;

public class Vehicle {

	@Resolvable(colName = "ID", formatter = IntFormatter.class)
	private Integer ID;
	@Resolvable(colName = "Modelo")
	private String model;
	@Resolvable(colName = "Ano")
	private String year;
	@Resolvable(colName = "Cor")
	private String color;
	@Resolvable(colName = "Marca")
	private String brand;
	@Resolvable(colName = "Motor")
	private String engine;
	private EnumCategory category;
	@Resolvable(colName = "Placa")
	private String licensePlate;
	@Resolvable(colName = "Valor")
	private Float value;
	
	private EnumCity location;
	
	
	private List<Rent> rents;
	private List<Reservation> reservations;
	
	public Vehicle() {
		this.reservations = new ArrayList<>();
		this.rents = new ArrayList<>();
	}

	public Vehicle(Integer ID){
		this.reservations = new ArrayList<>();
		this.rents = new ArrayList<>();
		this.ID = ID;
	}
	public Vehicle(String model, String year, String color, String brand, String engine, EnumCategory category, String licensePlate, EnumCity location) {
		super();
		this.model = model;
		this.year = year;
		this.color = color;
		this.brand = brand;
		this.engine = engine;
		this.category = category;
		this.licensePlate = licensePlate;
		this.location = location;
		this.reservations = new ArrayList<>();
		this.rents = new ArrayList<>();
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer ID) {
		this.ID = ID;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public EnumCategory getCategory() {
		return category;
	}

	public void setCategory(EnumCategory category) {
		this.category = category;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public EnumCity getLocation() {
		return location;
	}

	public void setLocation(EnumCity location) {
		this.location = location;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public void addRent(Rent rent) {
		rents.add(rent);
	}

	public void removeRent(Rent rent) {
		if (!rents.isEmpty())
			rents.remove(rent);
	}


	public void addReservation(Reservation reservation) {
		reservations.add(reservation);
	}

	public void removeReservation(Reservation reservation) {
		if (!reservations.isEmpty())
			reservations.remove(reservation);
	}


	public boolean isAvaibleToReservate(Period period){
		if(period.getLocation()==getLocation()){
			if(areRented())
				if(getLastRent().getContractPeriod().getEndDate().before(period.getStartDate())){
						if(getLastRent().isLate()) return false;
				}else return false;
							
				return hasReservationOverlaps(period);
		} else return false;
	}
	
	private boolean hasReservationOverlaps(Period period){
		if(!reservations.isEmpty()){
			for (Reservation reservation : reservations) {
				if(reservation.isOverlap(period)) return false;
			}
		} 
		return true;
	}
	
	public boolean isAvaibleToRent(Period period){
		if(period.getLocation()==getLocation()){
			if(areRented()) return false;
			else return hasReservationOverlaps(period);
		} else return false;
	}
	
	public Rent getLastRent(){
		return rents.get(rents.size()-1);
	}
	
	public boolean areRented(){
		boolean flag = false;
		if(!rents.isEmpty()){
			if(getLastRent().getRunningPeriod().getEndDate()==null)
				flag = true;
		}
		return flag;
	}
	
	@Override
	public String toString() {
		return model + " | Ano " + year + " | Cor " + color + " | Marca: " + brand + " | Motor " + engine;
	}

}
