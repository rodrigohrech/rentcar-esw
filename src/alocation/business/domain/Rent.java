package alocation.business.domain;

import java.util.Date;

public class Rent extends Transaction {

	private Period runningPeriod;
	
	
	public Rent() {}

	public Rent(Integer iD) {
		super(iD);
	}

	public Rent(Client client, Vehicle vehicle, Payment payment, Period contractPeriod) {
		super(client, vehicle, payment, contractPeriod);
	}

	public Period getRunningPeriod() {
		return runningPeriod;
	}

	public void setRunningPeriod(Period runningPeriod) {
		this.runningPeriod = runningPeriod;
	}

	public int compareTo(Rent rent) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isLate() {
		boolean flag = false;
		Date today = new Date();
		if(runningPeriod.getEndDate()== null){
			if(today.after(getContractPeriod().getEndDate())) flag = true;
		}
		return flag;
	}

}
