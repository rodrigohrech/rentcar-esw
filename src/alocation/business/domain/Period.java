package alocation.business.domain;

import java.util.Date;

import com.towel.el.annotation.Resolvable;

import alocation.util.DateFormatter;
import alocation.util.EnumCityFormatter;
import alocation.util.IntFormatter;

public class Period {

	@Resolvable(colName="Localizacao", formatter=EnumCityFormatter.class)
	private EnumCity location;
	@Resolvable(colName="Inicio", formatter=DateFormatter.class) 
	private Date startDate;
	@Resolvable(colName="Fim", formatter=DateFormatter.class) 
	private Date endDate;
	

	public Period() {}
	

	public Period(Date startDate, Date endDate, EnumCity location) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.location = location;
	}

	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public EnumCity getLocation() {
		return location;
	}


	public void setLocation(EnumCity location) {
		this.location = location;
	}


	public int compareTo(Period period){
		int result;
		if((startDate == period.getStartDate()) && (endDate == period.getEndDate())) result = 0;
		else if(isOverlap(period)) result = 1;//Overlaps Or CitiesNotEqual
		else result = -1;
		return result;
	}
	
	public boolean isOverlap(Period period){
		return startDate.before(period.getEndDate())  &&  (endDate.after(period.getStartDate()));
	}
	
}
