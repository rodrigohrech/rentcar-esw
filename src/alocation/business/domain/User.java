package alocation.business.domain;

import com.towel.el.annotation.Resolvable;

import alocation.util.IntFormatter;

public abstract class User {

	@Resolvable(colName="ID", formatter=IntFormatter.class) 
	protected Integer ID;
	@Resolvable(colName="Nome") 
	protected String name;
	protected String phone;
	protected String address;
	protected String email;
	protected String password;
	
	public User(){}
	
	
	public User(String name, String phone, String address, String email, String password) {
		super();
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.email = email;
		this.password = password;
	}
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
