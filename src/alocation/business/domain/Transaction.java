package alocation.business.domain;

import com.towel.el.annotation.Resolvable;

import alocation.util.IntFormatter;

public abstract class Transaction {

	@Resolvable(colName="ID", formatter=IntFormatter.class) 
	private Integer ID;
	private Client client;
	private Vehicle vehicle;
	private Payment payment;
	private Period contractPeriod;
	
	public Transaction() {}
	
	public Transaction(Integer iD) {
		this.ID = iD;
	}

	public Transaction(Client client, Vehicle vehicle, Payment payment, Period contractPeriod) {
		this.client = client;
		this.vehicle = vehicle;
		this.payment = payment;
		this.contractPeriod = contractPeriod;
	}

	public abstract boolean isLate();
	
	public boolean isOverlap(Period period){
		return (contractPeriod.compareTo(period)>=0);
	}
	
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	public Period getContractPeriod() {
		return contractPeriod;
	}
	public void setContractPeriod(Period contractPeriod) {
		this.contractPeriod = contractPeriod;
	}
	
	
}
