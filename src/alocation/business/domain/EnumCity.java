package alocation.business.domain;

public enum EnumCity {

	POA("Porto Alegre"), SAO("Sao Paulo"), BSB("Brasilia"), NONE("Alugado");
	
	private final String city;
	
	EnumCity(String city) { this.city = city; }
	
	public String getCity() { return city; }
	
	public static EnumCity byType(String type){
		for(EnumCity types : values()){
			if (type  == types.getCity()) return types;
		}
		throw new IllegalArgumentException("Invalid Type");
	}
	
	public static String[] getAllCities(){
		    EnumCity[] cities = values();
		    String[] names = new String[cities.length];

		    for (int i = 0; i < cities.length; i++) {
		        names[i] = cities[i].getCity();
		    }

		    return names;
		}
}
