package alocation.business.domain;

public enum EnumReservationState {

	PENDING("Pendente"), APPROVED("Aprovado"), DENIED("Negado"), CANCELED("Cancelado");
	
	private final String type;
	
	EnumReservationState(String type) { this.type = type; }
	
	public String getType() { return type; }
	
	public static EnumReservationState byType(String type){
		for(EnumReservationState types : EnumReservationState.values()){
			if (type  == types.getType()) return types;
		}
		throw new IllegalArgumentException("Invalid Code");
	}
}
