package alocation.business.domain;

import java.util.ArrayList;
import java.util.List;

import com.towel.el.annotation.Resolvable;

public class Client extends User {
 
	private String numberCNH; 
	private String numberCPF;
	
	private List<Reservation> reservations;
	private List<Rent> rents;
	
	public Client(){
		this.reservations = new ArrayList<>();
		this.rents = new ArrayList<>();
	}
	
	public Client(String name, String phone, String address, String email, String password, String numberCNH,
			String numberCPF) {
		super(name, phone, address, email, password);
		this.numberCNH = numberCNH;
		this.numberCPF = numberCPF;
		this.reservations = new ArrayList<>();
		this.rents = new ArrayList<>();
	}

	public String getCNH() {
		return numberCNH;
	}

	public void setCNH(String numberCNH) {
		this.numberCNH = numberCNH;
	}

	public String getCPF() {
		return numberCPF;
	}

	public void setCPF(String numberCPF) {
		this.numberCPF = numberCPF;
	}

	public void addReservation(Reservation reservation){
		this.reservations.add(reservation);
	}
	
	public void removeReservation(Reservation reservation){
		if(!reservations.isEmpty())this.reservations.remove(reservation);
	}
	
	public void addRent(Rent rent){
		this.rents.add(rent);
	}
	
	public void removeRent(Rent rent){
		if(!rents.isEmpty())this.rents.remove(rent);
	}
	@Override
	public String toString() {
		return "Client [numberCNH=" + numberCNH + ", numberCPF=" + numberCPF + ", ID=" + ID + ", name=" + name
				+ ", phone=" + phone + ", address=" + address + ", email=" + email + ", password=" + password + "]";
	}
	
	
	
	
}
