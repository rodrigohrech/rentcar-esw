package alocation.business;

import java.util.List;

import alocation.business.domain.EnumCategory;
import alocation.business.domain.Period;
import alocation.business.domain.Vehicle;

public interface VehicleService {

	public List<Vehicle> getAllVehicles();
	
	public List<Vehicle> getAllVehiclesAvaibleToRent(EnumCategory category, Period period);

	public List<Vehicle> getAllVehiclesAvaibleToReservate(EnumCategory category, Period period);
	
	public void save(Vehicle vehicle);
	
	public void delete(Vehicle vehicle);
	
	public Vehicle get(Integer id);
}
