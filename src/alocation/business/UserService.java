package alocation.business;

import java.util.List;

import alocation.business.domain.Client;

public interface UserService {

	public List<Client> getAllClients();
	public Client createNewClient(Client client) throws BusinessException;
}
