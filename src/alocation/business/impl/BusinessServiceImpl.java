package alocation.business.impl;

import java.util.Date;

import alocation.business.BusinessException;
import alocation.business.BusinessService;
import alocation.business.domain.Client;
import alocation.business.domain.EnumCity;
import alocation.business.domain.EnumReservationState;
import alocation.business.domain.Payment;
import alocation.business.domain.Period;
import alocation.business.domain.Reservation;
import alocation.business.domain.Vehicle;
import alocation.data.DatabaseSingleton;
import alocation.data.interfaces.ReservationDAO;

public class BusinessServiceImpl implements BusinessService {

	DatabaseSingleton db;
	
	public BusinessServiceImpl(DatabaseSingleton db) {
		this.db = db;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void reservateVehicle(Client client, Vehicle vehicle, Payment payment, Period period) throws BusinessException {
		if ((client == null) || (vehicle == null) || (period == null) || (payment == null))
			throw new BusinessException("exception.invalid.data");

		if(!vehicle.isAvaibleToReservate(period)) throw new BusinessException("exception.vehicle.reservation.unavaible");
		
		Reservation reservation = new Reservation(client, vehicle, payment, period);
		reservation.setState(EnumReservationState.PENDING);
		client.addReservation(reservation);
		vehicle.addReservation(reservation);
		
		db.get(ReservationDAO.class).save(reservation);
	}

	@Override
	public void rentVehicle(Client client, Vehicle vehicle, Period period) throws BusinessException {
		// TODO Auto-generated method stub

	}

	@Override
	public void returnVehicle(Vehicle vehicle) throws BusinessException {
		// TODO Auto-generated method stub

	}

	@Override
	public Period createPeriod(Date startDate, Date endDate, EnumCity location) throws BusinessException {
		if ((startDate == null) || (endDate == null) || (location == null))
			throw new BusinessException("exception.invalid.data");
		if(startDate.after(endDate) || startDate.equals(endDate)) throw new BusinessException("exception.period.invalid");
		Period period = new Period(startDate, endDate, location);
		return period;
	}

}
