package alocation.business.impl;

import java.util.List;

import alocation.business.BusinessException;
import alocation.business.UserService;
import alocation.business.domain.Client;
import alocation.data.DatabaseSingleton;
import alocation.data.interfaces.ClientDAO;
import alocation.data.interfaces.GenericDAO;

public class UserServiceImpl implements UserService {

	private GenericDAO<Client> dao;

	@SuppressWarnings("unchecked")
	public UserServiceImpl(DatabaseSingleton db) {
		this.dao = db.get(ClientDAO.class);
	}
	
	@Override
	public List<Client> getAllClients() {
		return dao.getAll();
	}

	@Override
	public Client createNewClient(Client client) throws BusinessException {
		client = dao.save(client);
		return client;
	}

}
