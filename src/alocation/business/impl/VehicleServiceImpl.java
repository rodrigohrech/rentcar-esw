package alocation.business.impl;

import java.util.ArrayList;
import java.util.List;

import alocation.business.VehicleService;
import alocation.business.domain.EnumCategory;
import alocation.business.domain.Period;
import alocation.business.domain.Vehicle;
import alocation.data.DatabaseSingleton;
import alocation.data.interfaces.GenericDAO;
import alocation.data.interfaces.VehicleDAO;

public class VehicleServiceImpl implements VehicleService {

	private GenericDAO<Vehicle> dao;

	@SuppressWarnings("unchecked")
	public VehicleServiceImpl(DatabaseSingleton db) {
		this.dao = db.get(VehicleDAO.class);
	}

	@Override
	public List<Vehicle> getAllVehicles() {
		return dao.getAll();
	}


	@Override
	public List<Vehicle> getAllVehiclesAvaibleToRent(EnumCategory category, Period period) {
		List<Vehicle> vehicleList = new ArrayList<>();
		for (Vehicle vehicle : getAllVehicles()) {
			if ((vehicle.getCategory() == category || category.equals(EnumCategory.ALL))
					&& vehicle.isAvaibleToRent(period))
				vehicleList.add(vehicle);
		}
		return vehicleList;
	}

	@Override
	public List<Vehicle> getAllVehiclesAvaibleToReservate(EnumCategory category, Period period) {
		List<Vehicle> vehicleList = new ArrayList<>();
		for (Vehicle vehicle : getAllVehicles()) {
			if ((vehicle.getCategory() == category || category.equals(EnumCategory.ALL))
					&& vehicle.isAvaibleToReservate(period))
				vehicleList.add(vehicle);
		}
		return vehicleList;
	}
	
	@Override
	public void save(Vehicle vehicle) {
		this.dao.save(vehicle);
	}

	@Override
	public void delete(Vehicle vehicle) {
		this.dao.delete(vehicle);
	}

	@Override
	public Vehicle get(Integer ID) {
		return this.dao.get(new Vehicle(ID));
	}

	

}
