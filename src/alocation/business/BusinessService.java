package alocation.business;

import java.util.Date;

import alocation.business.domain.Client;
import alocation.business.domain.EnumCity;
import alocation.business.domain.Payment;
import alocation.business.domain.Period;
import alocation.business.domain.Vehicle;

public interface BusinessService {

	public void reservateVehicle(Client client, Vehicle vehicle, Payment payment, Period period) throws BusinessException;
	public void rentVehicle(Client client, Vehicle vehicle, Period period) throws BusinessException;
	public void returnVehicle(Vehicle vehicle) throws BusinessException;
	public Period createPeriod(Date startDate, Date endDate, EnumCity location) throws BusinessException;
	
}
