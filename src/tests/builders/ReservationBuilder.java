package tests.builders;

import java.util.Date;

import javax.print.attribute.standard.DateTimeAtCompleted;

import alocation.business.domain.Client;
import alocation.business.domain.Payment;
import alocation.business.domain.Reservation;
import alocation.business.domain.Vehicle;

public class ReservationBuilder {

	private Reservation reservation;
	
	public ReservationBuilder() {
		this.reservation = new Reservation();
	}
	
	public Reservation randomBuild(){
		Client c = ClientFactory.INSTANCE.getClients(1).get(0);
		Vehicle v = VehicleFactory.INSTANCE.getVehicles(1).get(0);
		Payment p = new Payment(1, new Date(),  (float) 111.00 , c, v);
		
		reservation.setClient(c);
		reservation.setVehicle(v);
		reservation.setPayment(p);
		
		c.addReservation(reservation);
		v.addReservation(reservation);

		return reservation;
	}
}
