package tests.builders;

import java.util.ArrayList;
import java.util.List;

import alocation.business.domain.Client;

public class ClientFactory {
	
	private List<Client> clientList;
	public static final ClientFactory INSTANCE = new ClientFactory();
	
	private ClientFactory() {
		
	}
	
	public List<Client> getClients(int n){
		clientList = new ArrayList<>();
		Client c;
		for (int i = 0; i < n; i++) {
			c = new Client("client"+i, "99999999", "Rua do Client "+i, "client"+i+"@email.com", "123456789", "987654321", "123123123");
			clientList.add(c);
		}
	
		return clientList;
	}
}
