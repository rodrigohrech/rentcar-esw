package tests.builders;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import alocation.business.domain.EnumCategory;
import alocation.business.domain.EnumCity;
import alocation.business.domain.Vehicle;

public class VehicleFactory {

	private List<Vehicle> vehicletList;
	public static final VehicleFactory INSTANCE = new VehicleFactory();
	
	private VehicleFactory() {
		
	}
	
	public List<Vehicle> getVehicles(int n){
		vehicletList = new ArrayList<>();
		Vehicle v;
		for (int i = 0; i < n; i++) {
			v = new Vehicle("model"+i, "200"+i, "color"+i, "brand"+i, "eng"+i, getRandomCategory(),"AAA-123"+i,getRandomCity());
			vehicletList.add(v);
		}
	
		return vehicletList;
	}
	
	public Vehicle getPOAVehicle(){
		return new Vehicle("model", "2000", "color", "brand", "eng", getRandomCategory(),"AAA-123",EnumCity.POA);
	}
	
	private EnumCategory getRandomCategory(){
		EnumCategory category;
		Random rand = new Random(); 
		int value = rand.nextInt(EnumCategory.getAllCategories().length);
		switch (value) {
		case 0:
			category = EnumCategory.BASIC;
			break;
		case 1:
			category = EnumCategory.LUXE;
			break;
		case 2:
			category = EnumCategory.VAN;
			break;
		default:
			category = EnumCategory.BASIC;
			break;
		}
		return category;
	}
	
	private EnumCity getRandomCity(){
		EnumCity city;
		Random rand = new Random(); 
		int value = rand.nextInt(EnumCity.getAllCities().length);
		switch (value) {
		case 0:
			city = EnumCity.POA;
			break;
		case 1:
			city = EnumCity.BSB;
			break;
		case 2:
			city = EnumCity.SAO;
			break;
		default:
			city = EnumCity.POA;
			break;
		}
		return city;
	}
}
