package tests.business.domain;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import alocation.business.domain.EnumCity;
import alocation.business.domain.Period;

public class PeriodTest {

	private Period periodA,periodB,periodC;
	
	@Before
	public void setUp() throws Exception {
		periodA = new Period(new Date(2015,11,1,13,00),new Date(2015, 11, 3, 13, 00),EnumCity.POA);
		periodB = new Period(new Date(2015,11,2,13,00),new Date(2015, 11, 4, 13, 00),EnumCity.POA);
		periodC = new Period(new Date(2015,11,4,13,00),new Date(2015, 11, 6, 13, 00),EnumCity.POA);
	}

	@Test
	public void Should_ReturnPositive_IfPeriodsOverlaps() {
		assertTrue(periodA.compareTo(periodB) > 0);
	}

	@Test
	public void Should_ReturnNegative_IfPeriodsDontOverlaps() {
		assertTrue(periodA.compareTo(periodC) < 0);
	}
	
	@Test
	public void Should_ReturnZero_IfPeriodsAreEquals() {
		assertTrue(periodA.compareTo(periodA) == 0);
	}
}
