package tests.business.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import alocation.business.domain.EnumCity;
import alocation.business.domain.Period;
import alocation.business.domain.Rent;
import alocation.business.domain.Reservation;
import alocation.business.domain.Vehicle;
import tests.builders.ClientFactory;
import tests.builders.VehicleFactory;

public class VehicleTest {

	private Vehicle vehicleEmpty, vehicleRentedLate, vehicleWithOnlyReservations, vehicleWithBoth;
	private Period periodStandart, periodOverlap, periodNoOverlap, pendingPeriod,periodNotLate;
	private Rent rentPending = new Rent();
	private Reservation reservationStandart;
	private int year = 115;
	private Date today = new Date();
	@Before
	public void setUp() throws Exception {
		vehicleEmpty = VehicleFactory.INSTANCE.getPOAVehicle();
		periodStandart = new Period(new Date(year,10,1,13,00),new Date(year, 10, 3, 13, 00),EnumCity.POA);
		periodOverlap = new Period(new Date(year,10,2,13,00),new Date(year, 10, 4, 13, 00),EnumCity.POA);
		periodNoOverlap = new Period(new Date(year,10,5,13,00),new Date(year, 10, 7, 13, 00),EnumCity.POA);
		pendingPeriod = new Period(new Date(year,10,1,13,00),null,EnumCity.POA);
		periodNotLate = new Period(new Date(year,10,1,13,00),new Date(today.getYear(),today.getMonth(),today.getDate()+2,13,00),EnumCity.POA);
		rentPending.setContractPeriod(periodStandart);
		rentPending.setRunningPeriod(pendingPeriod);
		vehicleRentedLate = VehicleFactory.INSTANCE.getPOAVehicle();
		vehicleRentedLate.addRent(rentPending);
		vehicleWithOnlyReservations = VehicleFactory.INSTANCE.getPOAVehicle();
		reservationStandart = new Reservation();
		reservationStandart.setContractPeriod(periodStandart);
		vehicleWithOnlyReservations.addReservation(reservationStandart);
	}

	@Test
	public void Should_ReturnTrue_IfReservationsAndRentsAreEmpty() {
		vehicleEmpty.setLocation(periodStandart.getLocation());
		assertTrue(vehicleEmpty.isAvaibleToReservate(periodStandart));
		
	}

	@Test
	public void Should_ReturnFalse_IfReservationsAndRentsAreEmpty() {
		vehicleEmpty.setLocation(EnumCity.BSB);
		assertFalse(vehicleEmpty.isAvaibleToReservate(periodStandart));
		
	}

	@Test
	public void Should_ReturnFalse_IfVehicleAreRented_LastDayContract_AFTER_FirstDayPeriod(){
		assertFalse(vehicleRentedLate.isAvaibleToReservate(periodOverlap));
	}
	
	@Test
	public void Should_ReturnFalse_IfVehicleAreRented_LastDayContract_BEFORE_FirstDayPeriod(){
		assertFalse(vehicleRentedLate.isAvaibleToReservate(periodNoOverlap));
	}
	
	@Test
	public void Should_ReturnFalse_IfVehicleAreRented_LastDayContract_Before_FirstDayPeriod_And_NotLate(){
		assertFalse(vehicleRentedLate.isAvaibleToReservate(periodNotLate));
	}
	
	@Test
	public void Should_ReturnFalse_IfVehicleHasReservationOverlap(){
		assertFalse(vehicleRentedLate.isAvaibleToReservate(periodOverlap));
	}
	
	@Test
	public void Should_ReturnTrue_IfVehicleHasNoReservationOverlap(){
		assertFalse(vehicleRentedLate.isAvaibleToReservate(periodNoOverlap));
	}
}
