package tests.business.domain;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import alocation.business.domain.EnumCity;
import alocation.business.domain.Payment;
import alocation.business.domain.Period;
import alocation.business.domain.Reservation;

public class ReservationTest {

	private Reservation reservation;
	private Period latePeriod, notLatePeriod;
	private Payment payment;
	
	@Before
	public void setUp() throws Exception {
		Date today = new Date();
		latePeriod = new Period(new Date(today.getYear(),today.getMonth(),today.getDate()-3,13,00),new Date(today.getYear(),today.getMonth(),today.getDate()-2,13,00),EnumCity.POA);
		notLatePeriod = new Period(new Date(today.getYear(),today.getMonth(),today.getDate()-3,13,00),new Date(today.getYear(),today.getMonth(),today.getDate()+2,13,00),EnumCity.POA);
		reservation = new Reservation();
		payment = new Payment();
	}


	@Test
	public void Should_ReturnFalse_When_ReservationIsNotLate() {
		reservation.setContractPeriod(notLatePeriod);
		assertFalse(reservation.isLate());
	}
	
	@Test
	public void Should_ReturnTrue_When_ReservationIsLate() {
		reservation.setContractPeriod(latePeriod);
		assertTrue(reservation.isLate());
	}
	
	@Test
	public void Should_ReturnTrue_When_ReservationIsPaid() {
		reservation.setPayment(payment);
		assertTrue(reservation.isPaid());
	}

	@Test
	public void Should_ReturnFalse_When_ReservationIsNotPaid() {
		assertFalse(reservation.isPaid());
	}
}
