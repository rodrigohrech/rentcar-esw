package tests.business.domain;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import alocation.business.domain.EnumCity;
import alocation.business.domain.Payment;
import alocation.business.domain.Period;
import alocation.business.domain.Vehicle;

public class PaymentTest {

	Period period;
	Vehicle vehicle;
	Payment payment;
	float expected = 20;
	@Before
	public void setUp() throws Exception {
		period = new Period(new Date(115,11,1,13,00),new Date(115, 11, 3, 13, 00),EnumCity.POA);
		vehicle = new Vehicle();
		vehicle.setValue((float) 10.00);
		payment = new Payment();
		payment.setReferencePeriod(period);
		payment.setVehicle(vehicle);
	}

	@Test
	public void Should_Return20_When_ValueIs10_And_2days() {
		assertTrue(expected == payment.getValue());
	}

}
