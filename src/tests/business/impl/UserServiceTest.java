package tests.business.impl;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import alocation.business.UserService;
import alocation.business.domain.Client;
import alocation.business.impl.UserServiceImpl;
import alocation.data.DatabaseSingleton;

public class UserServiceTest {

	private UserService service;

	@Before
	public void setUp() throws Exception {
		service = new UserServiceImpl(DatabaseSingleton.getINSTANCE());
	}
	
	@Test
	public void Should_ReturnAllClient_From_IntDataContent() {
		List<Client> list = service.getAllClients();
		assertTrue(list.size()>0);
	}

}
